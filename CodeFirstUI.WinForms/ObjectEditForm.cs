﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using CodeFirstUI.Framework;

namespace CodeFirstUI.WinForms
{
    public partial class ObjectEditForm : Form
    {
        private Type objectType;
        private object theObject;
        private object theClone;

        public ObjectEditForm()
        {
            InitializeComponent();
        }

        public ObjectEditForm(object obj)
        {
            // TODO: Complete member initialization
            objectType = obj.GetType();

            this.theObject = obj;
            this.theClone = CloneObject(obj);
            this.Tag = theClone;

            InitializeComponent();

            //this.bindingSource1.DataSource = objectType;
            //this.bindingSource1.Add(theClone);

            this.SuspendLayout();

            string label = NaturalLanguageSystem.GetLabel(objectType.Name);
            this.Text = string.Format("{0} Edit Form", label);
            WinformsUI.Setup(flowLayoutPanel1, theClone);

            this.ResumeLayout();

        }
        
        private void OK_button_Click(object sender, EventArgs e)
        {
            // write the values back to the object:
            CopyFields(theClone, theObject);
        }

        public static object CloneObject(object objectToCopy)
        {
            //object clone = objectToCopy.MemberwiseClone();        // won't compile
            object returnValue = null;
            Type t = objectToCopy.GetType();
            if (t.IsSerializable)
                returnValue = CopyBySerialization(objectToCopy);
            else
                returnValue = CopyFields(objectToCopy);
            return returnValue;
        }

        private static object CopyFields(object objectToCopy)
        {
            Type t = objectToCopy.GetType();
            object targetObject = Activator.CreateInstance(t);
            // now copy the fields:
            CopyFields(objectToCopy, targetObject);
            return targetObject;
        }

        private static void CopyFields(object objectToCopy, object targetObject)
        {
            Type t = objectToCopy.GetType();
            if (targetObject.GetType() != t)
                throw new ArgumentException("Trying to copy fields of incompatible types");
            var fields = t.GetFields(BindingFlags.Instance|BindingFlags.Public|BindingFlags.NonPublic);
            foreach (FieldInfo info in fields)
            {
                object value = info.GetValue(objectToCopy);
                info.SetValue(targetObject, value);
            }
        }

        private static object CopyBySerialization(object objectToCopy)
        {
            MemoryStream memoryStream = new MemoryStream();
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            binaryFormatter.Serialize(memoryStream, objectToCopy);
            memoryStream.Position = 0;
            object returnValue = binaryFormatter.Deserialize(memoryStream);
            memoryStream.Close();
            memoryStream.Dispose();
            return returnValue;
        } 

        private void Cancel_button_Click(object sender, EventArgs e)
        {
            // how can we reset state of object?
        }


        public object Value { get { return theClone; } }
    }
}
