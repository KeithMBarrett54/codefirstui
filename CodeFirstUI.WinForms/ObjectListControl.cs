﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CodeFirstUI.Framework;

namespace CodeFirstUI.WinForms
{
    public partial class ObjectListControl : UserControl
    {
        public ObjectListControl()
        {
            InitializeComponent();
        }
        
        public ObjectListControl(System.Collections.IList list)
        {
            _src.DataSource = list;
            InitializeComponent();
            this.dataGridView1.DataSource = _src;
        }

        private BindingSource _src = new BindingSource();

        public System.Collections.IList List
        {
            get { return _src.List; }
            set
            {
                _src.DataSource = value;
                this.dataGridView1.DataSource = _src;
            }
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            object targetObject = _src.AddNew();
            // edit it
            if (!UI.ShowEditDialog(targetObject))
                _src.CancelEdit();
        }

        private void editToolStripMenuItem_Click(object sender, EventArgs e)
        {
            object targetObject = _src.Current;
            if (!UI.ShowEditDialog(targetObject))
                _src.CancelEdit();
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // confirm with user
            _src.RemoveCurrent();
        }

    }
}
