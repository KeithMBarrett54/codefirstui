﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Data;
using CodeFirstUI;
using System.Diagnostics;
using System.ComponentModel;
using CodeFirstUI.Framework;

namespace CodeFirstUI.WinForms
{
   
    public class WinformsUI: UIImplementation
    {
        public override void Initialise()
        {
            // Standard Winforms Initialisation
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
        }

        public override bool ShowEditDialog(object obj)
        {
            var form = new ObjectEditForm(obj);
            DialogResult res = form.ShowDialog();
            return (res == DialogResult.OK);
        }

        public void AddControls(Panel panel, object obj)
        {
            // for each public property of the class, add a control:
            Type type = obj.GetType();
            AddControls(panel, obj, type);
        }

        private void AddControls(Panel panel, object obj, Type type)
        {
            panel.Tag = obj;
            var properties = type.GetProperties();
            foreach (PropertyInfo property in properties)
            {
                if (IsInvisible(property))
                    continue;
                Control control = GetLabelledControl(property, obj);
                if (control != null)
                    panel.Controls.Add(control);
            }
        }

        const int borderWidth = 1;
        const int defaultLabelWidth = 120;
        const int defaultLabelHeight = 18;
        const int defaultControlWidth = 220;

        internal Control GetLabelledControl(PropertyInfo info, object obj)
        {
            Control labelledControl = null;
            string properyName = info.Name;
            LabelOption labelOption = GetLabelOption(info);
            switch(labelOption)
            {
            case LabelOption.None:
                labelledControl = GetSimpleControl(info, obj);
                Size s = labelledControl.Size;
                s.Width = defaultLabelWidth + defaultControlWidth;
                labelledControl.Size = s;
                break;
            case LabelOption.Group:
                labelledControl = CreateGroup(info, obj);
                break;
            case LabelOption.Above:
                labelledControl = GetLabelledSimpleControl(info, obj, true);
                break;
            case LabelOption.Default:
            case LabelOption.Left:
            default:
                labelledControl = GetLabelledSimpleControl(info, obj, false);
                break;
            }
            return labelledControl;
        }

        private Control CreateGroup(PropertyInfo info, object obj)
        {
            var groupBox1 = new System.Windows.Forms.GroupBox();
            //groupBox1.Location = new System.Drawing.Point(47, 321);
            groupBox1.Name = info.Name + "_grp";
            groupBox1.AutoSize = true;
            groupBox1.TabIndex = 5;
            groupBox1.TabStop = false;
            groupBox1.Text = GetLabel(info);
            //groupBox1.BackColor = Color.Beige;

            Control control = GetSimpleControl(info, obj);

            // position the control within the group box
            control.Location = new Point(10, 20);
            groupBox1.Controls.Add(control);

            // resize the group box to contain the control:
            Size s = control.Size;
            s += new Size(20, 20);
            groupBox1.Size = s;
            groupBox1.MinimumSize = s;
            groupBox1.MaximumSize = s;

            return groupBox1;
        }

        private static object GetValue(PropertyInfo info, object obj)
        {
            object value = info.GetValue(obj, null);
            if (value == null)
            {
                value = Activator.CreateInstance(info.PropertyType);
                info.SetValue(obj, value, null);
            }
            return value;
        }

        private FlowLayoutPanel CreateFlowPanel(PropertyInfo info, object obj)
        {
            FlowLayoutPanel panel = new System.Windows.Forms.FlowLayoutPanel();
            panel.Anchor = ((System.Windows.Forms.AnchorStyles)((((
                System.Windows.Forms.AnchorStyles.Top
                | System.Windows.Forms.AnchorStyles.Bottom)
                | System.Windows.Forms.AnchorStyles.Left)
                | System.Windows.Forms.AnchorStyles.Right)));
            panel.Location = new System.Drawing.Point(3, 15);
            panel.Name = info.Name + "_flowPanel";
            int panelWidth = defaultLabelWidth + defaultControlWidth + 2 * borderWidth + 6;
            int propertyCount = info.PropertyType.GetProperties().GetLength(0);
            int panelHeight = (defaultLabelHeight+12)*propertyCount;
            panel.Size = new System.Drawing.Size(panelWidth, panelHeight);
            //panel.MinimumSize = panel.Size;
            panel.MaximumSize = panel.Size;

            panel.AutoSize = true;            
            panel.AutoScroll = true;
            panel.TabIndex = 0;
            //panel.BackColor = Color.Lavender;
            panel.FlowDirection = FlowDirection.TopDown;

            AddControls(panel, obj, info.PropertyType);

            return panel;
        }

        private Panel GetLabelledSimpleControl(PropertyInfo info, object obj, bool labelAbove)
        {
            Label label = CreateLabel(info);
            Control control = GetSimpleControl(info,obj);
            Panel panel = CreatePanel(info, label, control, labelAbove);
            return panel;
        }

        private Control GetSimpleControl(PropertyInfo info, object obj)
        {
            Control control = null;
            ControlType type = GetControlType(info);
            switch (type)
            {
                case ControlType.StaticText:
                    control = CreateStaticText(info, obj);
                    break;

                case ControlType.Link:
                    control = CreateLink(info,obj);
                    break;

                case ControlType.CheckBox:
                    control = CreateCheckBox(info, obj);
                    break;

                case ControlType.CheckBoxList:
                    control = CreateCheckBoxList(info, obj);
                    break;

                case ControlType.MaskedTextBox:
                    control = CreateMaskedTextBox(info, obj);
                    break;

                case ControlType.ComboBox:
                    control = CreateComboBox(info, obj);
                    break;

                case ControlType.NumericUpDown:
                    control = CreateNumericUpDown(info, obj);
                    break;

                case ControlType.TextWithEdit:
                    control = CreateTextWithEdit(info, obj);
                    break;

                case ControlType.TextWithSelect:
                    control = CreateTextWithSelect(info, obj);
                    break;

                case ControlType.Inline:
                    control = CreateInlineEdit(info, obj);
                    break;

                case ControlType.ListBox:
                    control = CreateListBox(info, obj);
                    break;

                case ControlType.TextBox:
                default:
                    control = CreateTextBox(info, obj);
                    break;
            }
            return control;
        }

        private Control CreateListBox(PropertyInfo info, object obj)
        {
            object prop = info.GetValue(obj, null);
            System.Collections.IList list = null;
            if( prop is System.Collections.IList )
                list = prop as System.Collections.IList;
            var listBox = new ObjectListControl(list);
            listBox.Name = info.Name + "_textBox";
            int width = defaultLabelWidth + defaultControlWidth - 20;
            int height = defaultLabelHeight * 12;
            listBox.Size = new System.Drawing.Size(width, height);
            return listBox;
        }

        private Control CreateTextWithEdit(PropertyInfo info, object obj)
        {
            System.EventHandler eventHander = new System.EventHandler(this.EditButtonClick);

            return CreateTextBoxWithButton(info, obj, eventHander);

        }

        private Control CreateTextBoxWithButton(PropertyInfo info, object obj, System.EventHandler eventHander)
        {
            var panel2 = new System.Windows.Forms.Panel();
            var textBox1 = new System.Windows.Forms.TextBox();
            var button1 = new System.Windows.Forms.Button();

            panel2.Location = new System.Drawing.Point(0, 0);
            panel2.Name = info.Name + "_panel2";
            panel2.Size = new System.Drawing.Size(defaultControlWidth, defaultLabelHeight);
            panel2.Margin = new Padding(0);
            panel2.Tag = obj;

            int buttonHeight = defaultLabelHeight + 1;
            int buttonWidth = defaultLabelHeight + 3;

            textBox1.Location = new System.Drawing.Point(0, 0);
            textBox1.Margin = new System.Windows.Forms.Padding(0);
            textBox1.Name = info.Name + "_textBox1";
            textBox1.ReadOnly = true;
            textBox1.Size = new System.Drawing.Size(defaultControlWidth - buttonWidth, defaultLabelHeight);

            BindControl(textBox1, "Text", info, obj);

            button1.Location = new System.Drawing.Point(defaultControlWidth - buttonWidth, 0);
            button1.Name = info.Name + "_button1";
            button1.Size = new System.Drawing.Size(buttonWidth, buttonHeight);
            button1.Text = "…";
            button1.UseVisualStyleBackColor = true;
            button1.Margin = new Padding(0);
            button1.Padding = new Padding(0);
            button1.Tag = info;
            button1.Click += eventHander;

            panel2.Controls.Add(textBox1);
            panel2.Controls.Add(button1);

            return panel2;
        }

        private void EditButtonClick(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            PropertyInfo info = (PropertyInfo)button.Tag;
            if (info != null)
            {
                object value = GetPropertyValue(button, info);
                if (value != null)
                {
                    var form = new ObjectEditForm(value);
                    DialogResult res = form.ShowDialog();
                    if (res == DialogResult.OK)
                    {
                        value = form.Value;
                        SetPropertyValue(button, info, value);
                        // find the text editor and set its value:
                        SetTextBox(button, value);
                    }
                }
            }
 
        }

        private static void SetTextBox(Button button, object value)
        {
            Control parent = button.Parent;
            TextBox textBox = parent.Controls[0] as TextBox;
            if (textBox != null)
                textBox.Text = (value != null) ? value.ToString() : "";
        }

        private void SelectButtonClick(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            PropertyInfo info = (PropertyInfo)button.Tag;
            if (info != null)
            {
                System.Collections.IList list = UI.GetListFor(info);
                if (list != null)
                {
                    //object value = GetPropertyValue(button, info);
                    var form = new ListForm(list);
                    DialogResult res = form.ShowDialog();
                    if (res == DialogResult.OK)
                    {
                        var value = form.SelectedValue;
                        SetPropertyValue(button, info, value);
                        SetTextBox(button, value);
                    }
                }
            }
        }

        private Control CreateTextWithSelect(PropertyInfo info, object obj)
        {
            Control control = null;
            PickerType type = GetPickerType(info);
            switch (type)
            {
                case PickerType.DatePicker:
                    control = CreateDatePicker(info, obj);
                    break;
                case PickerType.FilePicker:
                    control = CreateFilePicker(info, obj);
                    break;
                case PickerType.FontPicker:
                case PickerType.ColorPicker:
                    break;
                case PickerType.ListPicker:
                default:
                    control = CreateListPicker(info, obj);
                    break;

            }
            return control;
        }

        private Control CreateFilePicker(PropertyInfo info, object obj)
        {
            System.EventHandler eventHander = new System.EventHandler(this.FileButtonClick);
            return CreateTextBoxWithButton(info, obj, eventHander);
        }

        private void FileButtonClick(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            PropertyInfo info = (PropertyInfo)button.Tag;
            if (info != null)
            {

                string filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
                bool forSave = false;
                string value = "";

                FileNameAttribute filterAtt = GetFileNameAttribute(info);
                if (filterAtt != null)
                {
                    filter = filterAtt.Filter;
                    forSave = filterAtt.ForSave;
                }

                FileDialog fileDlg;

                if (forSave)
                {
                    fileDlg = new SaveFileDialog();
                }
                else
                {
                    fileDlg = new OpenFileDialog();
                }
                //fileDlg.Title = "Select " + context.PropertyDescriptor.DisplayName;
                fileDlg.Title = "Select ";
                fileDlg.FileName = (string)value;
                fileDlg.Filter = filter;

                if (fileDlg.ShowDialog() == DialogResult.OK)
                {
                    value = fileDlg.FileName;
                    SetPropertyValue(button, info, value);
                    SetTextBox(button, value);
                }
                fileDlg.Dispose();
            }
        }

        private Control CreateListPicker(PropertyInfo info, object obj)
        {
            System.EventHandler eventHander = new System.EventHandler(this.SelectButtonClick);
            return CreateTextBoxWithButton(info, obj, eventHander);
        }

        private Control CreateCheckBoxList(PropertyInfo info, object obj)
        {
            CheckedListBox checkedListBox1 = new CheckedListBox();
            checkedListBox1.Name = info.Name + "_checkboxlist";
            checkedListBox1.FormattingEnabled = true;
            checkedListBox1.Size = new System.Drawing.Size(175, 100);
            checkedListBox1.MinimumSize = checkedListBox1.Size;
            checkedListBox1.Tag = info;

            int value = (int)info.GetValue(obj, null);

            checkedListBox1.DisplayMember = "Key";
            Dictionary<string, object> dict = GetComboListItems(info);
            foreach (var item in dict)
            {
                int mask = (int)item.Value;
                checkedListBox1.Items.Add(item.Key, (value & mask) != 0);
            }

            // It is not possible to bind the checkbox list, so we rely
            // on the on validated event to copy the value back
            checkedListBox1.Validated += new System.EventHandler(this.checkedListBox_Validated);

            return checkedListBox1;
        }

        private void checkedListBox_Validated(object sender, EventArgs e)
        {
            CheckedListBox checkedListBox = (CheckedListBox)sender;
            PropertyInfo info = (PropertyInfo)checkedListBox.Tag;            
            // copy the value back
            int value = 0;
            Dictionary<string, object> dict = GetComboListItems(info);
            foreach (var item in checkedListBox.Items)
            {
                string key = item.ToString();
                int mask = (int)dict[key];
                if (checkedListBox.CheckedItems.Contains(item))
                    value |= mask;
            }
            SetPropertyValue(checkedListBox, info, value);
        }

        private static object GetPropertyValue(Control control, PropertyInfo info)
        {
            object value = null;
            object obj = null;  // the main forms object
            Panel parent = FindParentPanel(control);
            if (parent != null)
                obj = parent.Tag;
            if (obj != null)
                value = info.GetValue(obj, null);
            return value;
        }

        private static void SetPropertyValue(Control control, PropertyInfo info, object value)
        {
            object obj = null;  // the main forms object
            Panel parent = FindParentPanel(control);
            if (parent != null)
                obj = parent.Tag;
            if (obj != null)
                info.SetValue(obj, value, null);
        }

        private static Panel FindParentPanel(Control control)
        {
            Control parent = control.Parent;
            while (parent != null && !(parent is Panel))
                parent = parent.Parent;
            return (Panel)parent;
        }

        private static ObjectEditForm FindParentForm(Control control)
        {
            Control parent = control.Parent;
            while (parent != null && !(parent is ObjectEditForm))
                parent = parent.Parent;
            return (ObjectEditForm)parent;
        }

        private Control CreateLink(PropertyInfo info, object obj)
        {
            LinkLabel linkLabel1 = new LinkLabel();
            linkLabel1.AutoSize = true;
            linkLabel1.Name = info.Name + "_link";
            
            linkLabel1.Text = GetLabel(info);
            linkLabel1.LinkClicked += new LinkLabelLinkClickedEventHandler(this.linkLabel_LinkClicked);
            object value = GetValue(info, obj);
            linkLabel1.Links.Add(new LinkLabel.Link(0,255,value));
            return linkLabel1;
        }

        private void linkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            object obj = e.Link.LinkData;
            if( obj != null )
                ShowDialog(obj);
            e.Link.Visited = true;
        }

        private Control CreateInlineEdit(PropertyInfo info, object obj)
        {
            object value = GetValue(info, obj);
            FlowLayoutPanel panel = CreateFlowPanel(info, value);
            return panel;
        }

        private static string GetMask(PropertyInfo info)
        {
            string mask = "";
            MaskAttribute attr = GetMaskAttribute(info);
            if (attr != null)
                mask = attr.Mask;
            return mask;
        }

        private static Panel CreatePanel(PropertyInfo info, Label label, Control control, bool labelAbove)
        {
            int labelWidth = defaultLabelWidth;
            int labelHeight = defaultLabelHeight;
            int controlWidth = defaultControlWidth;
            int controlHeight = defaultLabelHeight;
            if( control != null )
                controlHeight = control.Size.Height;
            int labelLeft = borderWidth;
            int controlLeft = defaultLabelWidth + borderWidth;
            int controlTop = 0;            
            int panelWidth = labelWidth + controlWidth + 2 * borderWidth;
            int panelHeight = controlHeight + 2 * borderWidth;

            if (labelAbove)
            {
                labelWidth = panelWidth;
                controlTop = labelHeight + borderWidth;
                controlLeft = borderWidth;
                controlWidth = panelWidth;
                panelHeight = labelHeight + controlHeight + 3 * borderWidth;
            }

            Panel panel1 = new System.Windows.Forms.Panel();

            label.Location = new System.Drawing.Point(labelLeft, 0);
            label.Size = new System.Drawing.Size(labelWidth, labelHeight);
            panel1.Controls.Add(label);

            if (control != null)
            {
                control.Location = new System.Drawing.Point(controlLeft, controlTop);
                control.Size = new System.Drawing.Size(controlWidth, controlHeight);
                panel1.Controls.Add(control);
            }

            panel1.Location = new System.Drawing.Point(3, 3);
            panel1.Name = info.Name + "_panel";
            panel1.Size = new System.Drawing.Size(panelWidth, panelHeight);
            panel1.TabIndex = 2;
            //panel1.BackColor = Color.AliceBlue;
            panel1.Margin = new Padding(0);

            return panel1;
        }


        private Control CreateTextBox(PropertyInfo info, object obj)
        {
            TextBox textBox = new System.Windows.Forms.TextBox();
            //textBox.Location = new System.Drawing.Point(defaultLabelWidth + borderWidth, 0);
            textBox.Name = info.Name + "_textBox";
            textBox.Size = new System.Drawing.Size(defaultControlWidth, defaultLabelHeight);

            BindControl( textBox, "Text", info, obj);

            return textBox;
        }

        private static Control CreateStaticText(PropertyInfo info, object obj)
        {
            Label label1 = new Label();
            label1.Location = new System.Drawing.Point(279, 19);
            label1.Name = info.Name + "_text";
            label1.AutoSize = true;
            string text = info.GetValue(obj, null).ToString();
            label1.Text = text;
            return label1;
        }

        private void BindControl(Control control, string controlPropertyName, PropertyInfo info, object obj)
        {
            Binding b = new Binding(controlPropertyName, obj, info.Name);
            // by default we will update the control on property updates, and the data source on validation
            control.DataBindings.Add(b);
            b.Parse +=new ConvertEventHandler(this.binding_Parse);
        }

        public void binding_Parse(object sender, ConvertEventArgs e)
        {
            Debug.WriteLine("Parse");
            Type currentType = e.Value.GetType();
            if (currentType != e.DesiredType)
            {
                // We need to convert.  For some reason the Binding class does not do this!
                TypeConverter converter = TypeDescriptor.GetConverter(e.DesiredType);
                if (converter != null && converter.CanConvertFrom(currentType))
                    e.Value = converter.ConvertFrom(e.Value);
                //else
                //    throw new Exception(string.Format("Cannot convert {0} to {1}", currentType.Name, e.DesiredType.Name));
            }
        }

        private Control CreateMaskedTextBox(PropertyInfo info, object obj)
        {
            string mask = GetMask(info);
            MaskedTextBox maskedTextBox = new System.Windows.Forms.MaskedTextBox();
            //maskedTextBox.Location = new System.Drawing.Point(defaultLabelWidth + borderWidth, 0);
            maskedTextBox.Mask = mask;
            maskedTextBox.Name = info.Name + "_maskedTextBox";
            maskedTextBox.Size = new System.Drawing.Size(defaultControlWidth, defaultLabelHeight);
            BindControl(maskedTextBox, "Text", info, obj);
            return maskedTextBox;
        }

        private static Label CreateLabel(PropertyInfo info)
        {
            Label label = new System.Windows.Forms.Label();
            label.AutoSize = true;
            label.Name = info.Name + "_label";
            label.TabIndex = 0;
            label.Text = GetLabel(info);
            return label;
        }

        private Control CreateCheckBox(PropertyInfo info, object obj)
        {
            var checkBox1 = new System.Windows.Forms.CheckBox();
            //checkBox1.AutoSize = true;
            //checkBox1.Location = new System.Drawing.Point(3, 3);
            checkBox1.Name = info.Name + "_chk";
            checkBox1.Size = new System.Drawing.Size(253, 17);
            checkBox1.TabIndex = 0;
            checkBox1.Text = GetLabel(info);
            //checkBox1.UseVisualStyleBackColor = true;
            //checkBox1.BackColor = Color.AliceBlue;
            BindControl(checkBox1, "Checked", info, obj);
            return checkBox1;
        }

        private DateTimePicker CreateDatePicker(PropertyInfo info, object obj)
        {
            DateTimePicker dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            dateTimePicker1.Name = info.Name + "_datePicker";
            dateTimePicker1.Size = new System.Drawing.Size(defaultControlWidth, defaultLabelHeight);
            BindControl(dateTimePicker1, "Value", info, obj);
            return dateTimePicker1;
        }

        private NumericUpDown CreateNumericUpDown(PropertyInfo info, object obj)
        {
            NumericUpDown numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            numericUpDown1.Name = info.Name + "_numericUpDown";
            numericUpDown1.Size = new System.Drawing.Size(defaultControlWidth, defaultLabelHeight);
            SetRange(numericUpDown1, info);
            BindControl(numericUpDown1, "Value", info, obj);
            return numericUpDown1;
        }

        private static void SetRange(NumericUpDown numericUpDown1, PropertyInfo info)
        {
            switch (info.PropertyType.Name)
            {
                case "Int16":
                    numericUpDown1.Maximum = Int16.MaxValue;
                    numericUpDown1.Minimum = Int16.MinValue;
                    break;
                case "Int32":
                    numericUpDown1.Maximum = Int32.MaxValue;
                    numericUpDown1.Minimum = Int32.MinValue;
                    break;
                case "Decimal":
                    numericUpDown1.DecimalPlaces = 2;
                    numericUpDown1.Increment = 0.01m;
                    numericUpDown1.Maximum = Decimal.MaxValue;
                    numericUpDown1.Minimum = Decimal.MinValue;
                    break;
                case "Single":
                case "Double":
                    numericUpDown1.DecimalPlaces = 5;
                    numericUpDown1.Increment = 0.00001m;
                    break;
            }
            CodeFirstUI.Framework.RangeAttribute rangeAttr = GetUIRangeAttribute(info);
            if (rangeAttr != null)
            {
                numericUpDown1.Minimum = rangeAttr.Minimum;
                numericUpDown1.Maximum = rangeAttr.Maximum;
            }
            CodeFirstUI.Framework.IncrementAttribute incrementAttr = GetUIIncrementAttribute(info);
            if (incrementAttr != null)
            {
                numericUpDown1.DecimalPlaces = incrementAttr.DecimalPlaces;
                numericUpDown1.Increment = incrementAttr.Increment;
            }
        }

        private ComboBox CreateComboBox(PropertyInfo info, object obj)
        {
            ComboBox comboBox1 = new System.Windows.Forms.ComboBox();
            comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            comboBox1.FormattingEnabled = true;
            comboBox1.Name = info.Name + "_comboBox";

            //BindingContext context = new BindingContext();
            //comboBox1.BindingContext = context;
            Dictionary<string, object> dict = GetComboListItems(info);
            comboBox1.DisplayMember = "Key";
            comboBox1.ValueMember = "Value";
            var source = new BindingSource(dict, null);
            comboBox1.DataSource = source;

            //foreach (object o in dict)
            //    comboBox1.Items.Add(o);

            BindControl(comboBox1, "SelectedValue", info, obj);

            //comboBox1.SelectedIndexChanged += comboBox_SelectedIndexChanged;

            return comboBox1;
        }

        private static void comboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            //ComboBox comboBox = sender as ComboBox;
            //int index = comboBox.SelectedIndex;
            //object o = comboBox.SelectedItem;
            //object v = comboBox.SelectedValue;
            //BindingSource source = comboBox.DataSource as BindingSource;
            //int i = source.IndexOf(o);
            //source.Position = i;
        }

        internal static void ReportError(Exception e)
        {
            string message = string.Format("Exception: {0}", e.ToString());
            MessageBox.Show(message);
        }


 /*
        private void SetUpdateModes()
        {
            SetUpdateModes(this, ControlUpdateMode.Never, DataSourceUpdateMode.Never);
        }

        private void SetUpdateModes(Control control, ControlUpdateMode cu, DataSourceUpdateMode du)
        {
            try
            {
                if (control.DataBindings != null)
                {
                    control.CausesValidation = false;
                    for (int i = 0; i < control.DataBindings.Count; i++)
                    {
                        var b = control.DataBindings[i];
                        b.ControlUpdateMode = cu;
                        b.DataSourceUpdateMode = du;
                    }
                }
                foreach (Control c in control.Controls)
                    SetUpdateModes(c,cu,du);
            }
            catch (Exception e)
            {
                WinformsUI.ReportError(e);
            }
        }

        private void ReadValues(Control control)
        {
            try
            {
                if (control.DataBindings != null)
                {
                    //control.CausesValidation = false;
                    for (int i = 0; i < control.DataBindings.Count; i++)
                    {
                        var b = control.DataBindings[i];
                        b.ReadValue();
                    }
                }
                foreach (Control c in control.Controls)
                    ReadValues(c);
            }
            catch (Exception e)
            {
                WinformsUI.ReportError(e);
            }
        }

        private void WriteValues(Control control)
        {
            try
            {
                if (control.DataBindings != null)
                {
                    //control.CausesValidation = false;
                    for (int i = 0; i < control.DataBindings.Count; i++)
                    {
                        var b = control.DataBindings[i];
                        b.WriteValue();
                    }
                }
                foreach (Control c in control.Controls)
                    WriteValues(c);
            }
            catch (Exception e)
            {
                WinformsUI.ReportError(e);
            }
        }
*/

        public override bool ShowListDialog(System.Collections.IList list)
        {
            var form = new ListForm(list);
            form.ShowDialog();
            return true;
        }

        internal static void Setup(Panel panel, object obj)
        {
            WinformsUI ui = UI.implementation as WinformsUI;
            if (ui != null)
                ui.AddControls(panel, obj);
        }
    }
}
