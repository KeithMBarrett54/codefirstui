﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections.ObjectModel;
using CodeFirstUI.Framework;

namespace CodeFirstUI.WinForms
{
    public partial class ListForm : Form
    {
        public ListForm()
        {
            InitializeComponent();
        }

        BindingSource _src = new BindingSource();

        public ListForm(System.Collections.IList list)
        {
            // TODO: Complete member initialization
            _src.DataSource = list;
            InitializeComponent();
            this.dataGridView1.DataSource = _src;
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // create a new object
/*
            Type listType = _src.List.GetType();
            if (listType.IsGenericType && listType.GetGenericArguments().GetLength(0) > 0)
            {
                Type elementType = listType.GetGenericArguments()[0];
                object targetObject = Activator.CreateInstance(elementType);
                // edit it
                if (UI.ShowEditDialog(targetObject))
                {
                    // add it to list:
                    _src.Add(targetObject);
                }
            }
 */
            object targetObject = _src.AddNew();
            // edit it
            if (!UI.ShowEditDialog(targetObject))
                _src.CancelEdit();
        }

        private void editToolStripMenuItem_Click(object sender, EventArgs e)
        {
            object targetObject = _src.Current;
            if (!UI.ShowEditDialog(targetObject))
                _src.CancelEdit();
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // confirm with user
            _src.RemoveCurrent();
        }

        public object SelectedValue 
        { 
            get 
            {
                var rows = dataGridView1.SelectedRows;
                if (rows.Count == 1)
                    return rows[0].DataBoundItem;
                return null;
            } 
            set 
            { 
            } 
        }
    }
}
