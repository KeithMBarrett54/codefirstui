﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//using KMBFormsControlLibrary;
using CodeFirstFormsTest.Model;
using CodeFirstUIFramework;

namespace CodeFirstFormsTest
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            customer.Surname = "Nigma";
            customer.FirstName = "Anna";
            customer.gender = Customer.Gender.Female;
            customer.BirthDate = new Date(1938, 4, 1);
            //customer.Address = new Address();
            //customer.Address.Line1 = "42 Wallaby Terrace";
            customers.Add(customer);
        }
        
       // private static DbContext;
        private static Customer customer = new Customer();
        private static Product product = new Product();

        private void button1_Click(object sender, EventArgs e)
        {
            UI.ShowEditDialog(customer);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var form = new TestForm1();
            form.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var form = new PropertyGridForm(customer);
            form.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            UI.ShowEditDialog(product);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            var form = new PropertyGridForm(product);
            form.ShowDialog();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            var form = new TestForm2();
            form.ShowDialog();
        }

        private List<Customer> customers = new List<Customer>();

        private void CustomerList_btn_Click(object sender, EventArgs e)
        {
            UI.ShowListDialog(customers);
        }

        private List<Order> orders = new List<Order>();

        private void button7_Click(object sender, EventArgs e)
        {
            UI.ShowListDialog(orders);
        }
    }
}
