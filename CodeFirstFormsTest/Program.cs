﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using CodeFirstFormsTest.Model;
using CodeFirstUI.Framework;
using CodeFirstUI.WinForms;

namespace CodeFirstFormsTest
{
    static class Program
    {

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            UI.implementation = new WinformsUI();
            UI.Initialise();
            MainPage mainPage = new MainPage();
            UI.ShowDialog(mainPage);
        }
    }
}
