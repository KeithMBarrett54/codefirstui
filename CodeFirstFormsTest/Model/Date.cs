﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Globalization;
using System.Diagnostics;

namespace CodeFirstFormsTest.Model
{
    [TypeConverter(typeof(DateTypeConverter))]
    public struct Date
    {
        /// <summary>
        /// Count of days since 1 Jan 1900:
        /// </summary>
        private int _days;

        public Date(DateTime dt)
        {
            _days = (int)dt.ToOADate();
        }

        public Date(int year, int month, int day)
        {
            DateTime dt = new DateTime(year, month, day);
            _days = (int)(dt.ToOADate());
        }

        public DateTime DateTime
        {
            get
            {
                var d = DateTime.FromOADate((double)_days);
                return d;
            }
            set
            {
                _days = (int)value.ToOADate(); 
            }
        }

        public override string ToString()
        {
            string s = DateTime.ToShortDateString();
            return s;
        }

        public static Date Parse(string s)
        {
            DateTime d = DateTime.Parse(s);
            return new Date(d);
        }
    }
    
    /// <summary>
    /// Type converter for dates
    /// </summary>
    public class DateTypeConverter : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context,
           Type sourceType)
        {
            if (sourceType == typeof(string) || sourceType == typeof(DateTime))
                return true;
            return base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context,
           CultureInfo culture, object value)
        {
            Debug.WriteLine("converting to Date");
            if (value is string)
            {
                return Date.Parse(value as string);
            }
            else if (value is DateTime)
            {
                return new Date((DateTime)value);
            }
            return base.ConvertFrom(context, culture, value);
        }

        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            if (destinationType == typeof(string) || destinationType == typeof(DateTime))
                return true;
            return base.CanConvertTo(context, destinationType);
        }

        public override object ConvertTo(ITypeDescriptorContext context,
           CultureInfo culture, object value, Type destinationType)
        {
            Debug.WriteLine("converting from Date");
            if (destinationType == typeof(string))
            {
                return ((Date)value).ToString();
            }
            else if (destinationType == typeof(DateTime))
            {
                return ((Date)value).DateTime;
            } 
            return base.ConvertTo(context, culture, value, destinationType);
        }

        public override bool IsValid(ITypeDescriptorContext context, Object value)
        {
            return true;
        }
    }
}
