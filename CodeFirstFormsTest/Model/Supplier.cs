﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CodeFirstFormsTest.Model
{
    public class Supplier
    {
        public string Name { get; set; }
        public Address Address { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
