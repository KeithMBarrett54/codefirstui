﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using CodeFirstUI.Framework;

namespace CodeFirstFormsTest.Model
{
    public class TestPage
    {
        // simple text with no adornment
        public string simpleTextField { get; set; }

        // static and private properties are not shown:
        private string privateProperty { get; set; }
        public static string staticProperty { get; set; }

        // we can override the label
        [DisplayName("Another text field")]
        public string simpleTextField2 { get; set; }
        // text with a mask
        [Mask("(LLL) 000-0000")]
        public string maskedTextField { get; set; }
        // numeric fields are shown with an up-down control:
        public int integerField { get; set; }
        public decimal decimalField { get; set; }
        public double doubleField { get; set; }

        // you can override how a property is displayed:
        [Display(ControlType.TextBox)]
        public int another_integer { get; set; }

        // You can define the range and increment of a numeric with attributes:
        [Range(40.0, 200.0), Increment(0.1)]
        public float beatsPerMinute { get; set; }

        // We can also use a range from the component model namespace (framework 4.5 only)
        //[System.ComponentModel.RangeAttribute(0, 120)]
        //public int Age { get; set; }
        // booleans are shown as a check box:
        public bool booleanField { get; set; }
        // enumerations are shown as combo boxes:
        public enum TestEnum { Zero, One, Two, Three, Four, Five }
        public TestEnum enumField { get; set; }
        // we show a flags field as a set of check boxes
        [Flags]
        public enum TestFlags { Olives=1, Pepperoni=2, Capers=4, ExtraCheese=8, Anchovies=16, Chillies=32 }
        public TestFlags flagsField { get; set; }

        // DateTime fields are shown with a date picker:
        public DateTime a_date_time { get; set; }

        // We also support a pure Date class:
        public Date a_date { get; set; }
      
        // a struct will be shown as a text field with a pop-up editor
        //[Display(LabelOption.Group, ControlType.Inline)]
        public TimeOfDay timeOfDay { get; set; }
        
        // strings with the FileName attribut will display a file picker:
        [FileName(Filter = "XML files (*.xml)|*.xml|All files (*.*)|*.*", ForSave = false)]
        public string fileName { get; set; }

        // If there is a list for an object then we will get a list picker
        public Country country { get; set; }

        // This should display as a list control:
        public List<OrderLine> _orderLines = new List<OrderLine>();
        public List<OrderLine> OrderLines { get { return _orderLines; } }

        public TestPage()
        {
            beatsPerMinute = 120;
        }
    }
}
