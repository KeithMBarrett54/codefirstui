﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CodeFirstFormsTest.Model
{
    public class OrderLine
    {
        public Order order { get; set; }
        public Product product { get; set; }
        public int Quantity { get; set; }
    }
}
