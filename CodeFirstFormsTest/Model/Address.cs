﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CodeFirstUI.Framework;

namespace CodeFirstFormsTest.Model
{
    [Display(LabelOption.Group, ControlType.Inline)]
    public class Address
    {
        public string StreetAddress { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public string PostCode { get; set; } 
        public Country Country { get; set; }

        public override string ToString()
        {
            string s = "";
            s += StreetAddress + "\r\n";
            s += City + "\r\n";
            s += Region + "\r\n";
            s += PostCode + "\r\n";
            s += Country + "\r\n";
            return s;
        }
    }
}

