﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using CodeFirstFormsTest.Model;

namespace CodeFirstFormsTest.Model
{
    [Description("Customer class")]
    public class Customer
    {
        [Description("Family Name")]
        public string Surname { get; set; }
        [DisplayName("First name")]
        public string FirstName { get; set; }
        public Date BirthDate { get; set; }
        public enum Gender { Male, Female };
        public Gender gender { get; set; }
        public bool Married { get; set; }
        public Address Address { get; set; }
        public string email { get; set; }

        public override string ToString()
        {
            return Surname + ", " + FirstName;
        }
    }
}
