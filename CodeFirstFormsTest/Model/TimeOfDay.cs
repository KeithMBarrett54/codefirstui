﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Globalization;
using System.Diagnostics;
using CodeFirstUI.Framework;

namespace CodeFirstFormsTest.Model
{
    [TypeConverter(typeof(TimeOfDayTypeConverter))]
    public struct TimeOfDay
    {
        private int totalSeconds;

        public TimeOfDay(UInt16 hours, UInt16 minutes, UInt16 seconds)
        {
            totalSeconds = 0;
            Set(hours, minutes, seconds);
        }

        private void Set(UInt16 hours, UInt16 minutes, UInt16 seconds)
        {
            totalSeconds = ((hours * 60) + minutes) * 60 + seconds;
        }

        [Range(0,23)]
        public UInt16 Hours
        {
            get { return (UInt16)(totalSeconds / 3600); }
            set { Set(value, Minutes, Seconds); }
        }

        [Range(0, 59)]
        public UInt16 Minutes
        {
            get { return (UInt16)((totalSeconds / 60) % 60); }
            set { Set(Hours, value, Seconds); }
        }

        [Range(0, 59)]
        public UInt16 Seconds
        {
            get { return (UInt16)(totalSeconds % 60); }
            set { Set(Hours, Minutes, value); }
        }

        public override string ToString()
        {
            return String.Format("{0:D2}:{1:D2}:{2:D2}", Hours, Minutes, Seconds);
        }

        internal static TimeOfDay Parse(string p)
        {
            string[] list = p.Split(':');
            UInt16 hours = (list.Length > 0) ? UInt16.Parse(list[0]) : (UInt16)0;
            UInt16 minutes = (list.Length > 1) ? UInt16.Parse(list[1]) : (UInt16)0;
            UInt16 seconds = (list.Length > 2) ? UInt16.Parse(list[2]) : (UInt16)0;
            return new TimeOfDay(hours, minutes, seconds);
        }
    }

    /// <summary>
    /// Type converter for dates
    /// </summary>
    public class TimeOfDayTypeConverter : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context,
           Type sourceType)
        {
            if (sourceType == typeof(string) || sourceType == typeof(DateTime))
            {
                return true;
            }
            return base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context,
           CultureInfo culture, object value)
        {
            if (value is string)
            {
                Debug.WriteLine("converting string to TimeOfDay");
                TimeOfDay d = TimeOfDay.Parse(value as string);
                return d;
            }
            else if (value is DateTime)
            {
                DateTime dateTime = (DateTime)value;
                UInt16 hours = (UInt16)dateTime.Hour;
                UInt16 minutes = (UInt16)dateTime.Minute;
                UInt16 seconds = (UInt16)dateTime.Second;
                return new TimeOfDay(hours, minutes, seconds);
            }
            return base.ConvertFrom(context, culture, value);
        }

        public override object ConvertTo(ITypeDescriptorContext context,
           CultureInfo culture, object value, Type destinationType)
        {
            if (destinationType == typeof(string))
            {
                Debug.WriteLine("converting TimeOfDay to string");
                return ((TimeOfDay)value).ToString();
            }
            else if (destinationType == typeof(DateTime))
            {
                TimeOfDay v = (TimeOfDay)value;
                return new DateTime(0, 0, 0, v.Hours, v.Minutes, v.Seconds);
            }
            return base.ConvertTo(context, culture, value, destinationType);
        }
    }
}
