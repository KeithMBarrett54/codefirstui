﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CodeFirstFormsTest.Model
{
    public class DatabaseContext
    {
        public List<Country> countries = new List<Country>();
        public List<Customer> customers = new List<Customer>();
        public List<Supplier> suppliers = new List<Supplier>();
        public List<Product> products = new List<Product>();
        public List<Order> orders = new List<Order>();

       public void Load()
       {
           CreateCountries(); 
           CreateCustomers();
       }

       private void CreateCountries()
       {
           countries.Add(new Country() { Name = "United Kingdom", ISOCode = "GB" });
           countries.Add(new Country() { Name = "France", ISOCode = "FR" });
           countries.Add(new Country() { Name = "USA", ISOCode = "US" });
       }

       private void CreateCustomers()
       {
           customers.Add( new Customer()
           {
               Surname = "Nigma",
               FirstName = "Anna",
               gender = Customer.Gender.Female,
               BirthDate = new Date(1917, 2, 6),
               //Address = new Address();
               //Address.Line1 = "42 Wallaby Terrace";
           });
       }


    }
}
