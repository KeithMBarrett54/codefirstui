﻿
using CodeFirstUI.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CodeFirstFormsTest.Model
{
    public enum ProductTypeEnum { NotSpecified, Vinyl, CD, DVD, Download, Book, Poster };

    public class Product
    {
        //[Key]
        public string ProductCode { get; set; }
        public ProductTypeEnum ProductType { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public int StockLevel { get; set; }
        public Supplier Supplier { get; set; }
        [Display(LabelOption.Above)]
        [FileName(Filter = "JPEG files (*.jpg)|*.jpg|All files (*.*)|*.*", ForSave = false)]
        public string ImageFileName { get; set; }

        public override string ToString()
        {
            return ProductCode + ": " + Title;
        }
    }
}
