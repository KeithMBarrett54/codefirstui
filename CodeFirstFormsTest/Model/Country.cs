﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CodeFirstFormsTest.Model
{
    public class Country
    {
        public string Name { get; set; }
        /// <summary>
        /// ISO 3166 2 character code
        /// </summary>
        public string ISOCode { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
