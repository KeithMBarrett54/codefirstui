﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CodeFirstFormsTest.Model
{
    public class Order
    {
        // This should be shown with a button to select the customer
        public Customer customer { get; set; }
        public Date date { get; set; }
        // This should be a list control:
        public List<OrderLine> _orderLines = new List<OrderLine>();
        public List<OrderLine> OrderLines { get { return _orderLines; } }
    }
}
