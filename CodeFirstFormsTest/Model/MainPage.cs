﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using CodeFirstUI.Framework;

namespace CodeFirstFormsTest.Model
{
    /// <summary>
    /// The entry point for the application.
    /// </summary>
    [Description("Main page of the application")]
    public class MainPage
    {
        /// <summary>
        /// Static constructor
        /// </summary>
        static MainPage()
        {
            RegisterLists();
            context.Load();
        }

        private static DatabaseContext context = new DatabaseContext();

        private static void RegisterLists()
        {
            UI.RegisterList(typeof(Country), context.countries);
            UI.RegisterList(typeof(Customer), context.customers);
            UI.RegisterList(typeof(Supplier), context.suppliers);
            UI.RegisterList(typeof(Product), context.products);
            UI.RegisterList(typeof(Order), context.orders);
        }
 
        [Display(LabelOption.None, ControlType.StaticText)]
        public string Instructions 
        { 
            get 
            { 
                return "This page is the main entry point to the application.  "
                    +"It contains links to other forms which show how we can create a "
                    +"user interface purely by interpreting the properties and attributes "
                    +"of 'POCO' objects.\n"
                    +"Click on the links below.\n\n"; 
            }
        }

        private TestPage _testPage = new TestPage();
        [DisplayName("Test Page")]
        [Display(LabelOption.None, ControlType.Link)]
        public TestPage testPage { get { return _testPage; } }

        [DisplayName("Customer List")]
        [Display(LabelOption.None,ControlType.Link)]
        public List<Customer> customers { get { return context.customers; } }

        [DisplayName("Supplier List")]
        [Display(LabelOption.None, ControlType.Link)]
        public List<Supplier> suppliers { get { return context.suppliers; } }

        [DisplayName("Product List")]
        [Display(LabelOption.None, ControlType.Link)]
        public List<Product> products { get { return context.products; } }

        [DisplayName("Order List")]
        [Display(LabelOption.None, ControlType.Link)]
        public List<Order> orders { get { return context.orders; } }



    }
}
