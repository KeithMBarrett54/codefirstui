﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CodeFirstUI.Framework
{
    public class MaskAttribute : Attribute
    {
        public MaskAttribute(string m)
        {
            Mask = m;
        }

        public string Mask { get; set; }
    }
}
