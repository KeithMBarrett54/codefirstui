﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CodeFirstUI.Framework
{
    public enum LabelOption { Default, None, Left, Above, Group }
    public enum ControlType
    {
        Default, TextBox, MaskedTextBox, MultilineTextBox, CheckBox, CheckBoxList, RadioButton,
        ComboBox, ListBox, NumericUpDown, Inline, TextWithEdit, TextWithSelect,
        Link, Button,
        StaticText,
       
    }

    public enum PickerType
    {
        DatePicker,
        FilePicker,
        FontPicker,
        ColorPicker,
        ListPicker,
    }

    public enum TextType
    {
        SingleLine,
        Masked,
        MultiLine,
        RichText,
    }

    public static class UI
    {
        public static  UIImplementation implementation { get; set; }

        public static bool ShowEditDialog(object obj)
        {
            return implementation.ShowEditDialog(obj);
        }

        public static bool ShowListDialog(System.Collections.IList list)
        {
            return implementation.ShowListDialog(list);
        }

        public static void Initialise()
        {
            implementation.Initialise();
        }

        public static void ShowDialog(object obj)
        {
            if (obj is System.Collections.IList)
                ShowListDialog(obj as System.Collections.IList);
            else
                ShowEditDialog(obj);
        }

        private static Dictionary<Type, System.Collections.IList> listDictionary = new Dictionary<Type, System.Collections.IList>();

        public static void RegisterList(Type elementType, System.Collections.IList list)
        {
            listDictionary.Add(elementType, list);
        }

        public static System.Collections.IList GetListFor(System.Reflection.PropertyInfo info)
        {
            Type type = info.PropertyType;
            if (listDictionary.ContainsKey(type))
                return listDictionary[type];
            return null;
        }
    }
}
