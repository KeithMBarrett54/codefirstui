﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CodeFirstUI.Framework
{
    [AttributeUsage(AttributeTargets.Class|AttributeTargets.Struct|AttributeTargets.Property)]
    public class DisplayAttribute : Attribute
    {
        public DisplayAttribute(LabelOption lo, ControlType ct=ControlType.Default)
        {
            LabelOption = lo;
            ControlType = ct;
        }

        public DisplayAttribute(ControlType ct)
        {
            LabelOption = LabelOption.Default;
            ControlType = ct;
        }

        public LabelOption LabelOption { get; set; }
        public ControlType ControlType { get; set; }
    }
}
