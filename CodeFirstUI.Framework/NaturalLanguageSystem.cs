﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CodeFirstUI.Framework
{
    public class NaturalLanguageSystem
    {
        public static string GetLabel(System.Reflection.PropertyInfo info)
        {
            return GetLabel(info.Name);
        }

        public static string GetLabel(string name)
        {
            // we could check for resource.  If not found return the label (de-camelbacked)
            return DeCamelback(name);
        }

        private static string DeCamelback(string name)
        {
            string label = "";
            bool toUpper = true;
            foreach (char c in name)
            {
                if (c == '_')
                {
                    if (label.Length > 0)
                        label += ' ';
                    toUpper = true;
                    continue;
                }
                if (Char.IsUpper(c) && label.Length > 0)
                    label += ' ';
                if (toUpper)
                    label += char.ToUpper(c);
                else
                    label += c;
                toUpper = false;
            }
            return label;
        }

    }
}
