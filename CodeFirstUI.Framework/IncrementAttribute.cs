﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CodeFirstUI.Framework
{
    public class IncrementAttribute : Attribute
    {
        public decimal Increment { get; set; }

        public IncrementAttribute(double inc)
        {
            Increment = (decimal)inc;
        }

        public int DecimalPlaces 
        { 
            get 
            {
                int places = 0;
                double x = Math.Log10((double)Increment);
                if (x < 0.0)
                    places = (int)Math.Floor(-x);
                return places;
            } 
        }
    }
}
