﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.ComponentModel;

namespace CodeFirstUI.Framework
{
    public abstract class UIImplementation
    {
        public abstract void Initialise();
        public abstract bool ShowEditDialog(object obj);
        public abstract bool ShowListDialog(System.Collections.IList list);

        public virtual bool ShowDialog(object obj)
        {
            if (obj is System.Collections.IList)
                return ShowListDialog(obj as System.Collections.IList);
            else
                return ShowEditDialog(obj);
        }

        protected bool IsInvisible(PropertyInfo property)
        {
            var getter = property.GetGetMethod();
            if (getter != null)
            {
                if (getter.IsPrivate || getter.IsStatic)
                    return true;
            }
            return false;
        }

        protected ControlType GetControlType(PropertyInfo info)
        {
            ControlType controlOption = ControlType.Default;

            // does the property have a Display attribute?
            DisplayAttribute attr = GetDisplayAttribute(info);
            if (attr != null)
                controlOption = attr.ControlType;
            if (controlOption != ControlType.Default)
                return controlOption;

            // does the type have a display attribute?
            Type type = info.PropertyType;
            attr = GetDisplayAttribute(type);
            if (attr != null)
                controlOption = attr.ControlType;
            if (controlOption != ControlType.Default)
                return controlOption;

            if (type.IsEnum)
            {
                if (IsFlagsType(type))
                    return ControlType.CheckBoxList;
                else
                    return ControlType.ComboBox;
            }

            switch (type.Name)
            {
                case "Boolean":
                    return ControlType.CheckBox;
                case "Int16":
                case "Int32":
                case "UInt16":
                case "UInt32":
                case "Decimal":
                case "Single":
                case "Double":
                    return ControlType.NumericUpDown;
                case "FileInfo":
                case "DateTime":
                case "Date":
                case "Font":
                case "Color":
                    return ControlType.TextWithSelect;
                case "String":
                    if (HasMaskAttribute(info))
                        return ControlType.MaskedTextBox;
                    else if (HasFileNameAttribute(info))
                        return ControlType.TextWithSelect;
                    else
                        return ControlType.TextBox;
                default:
                    // unknown object type
                    if (type.IsValueType)
                        return ControlType.TextWithEdit;
                    else if (type.IsArray || HasInterface(type, typeof(System.Collections.IList)) )
                        return ControlType.ListBox;
                    else
                        return ControlType.TextWithSelect;
            }
        }

        private bool HasInterface(Type type, Type iface)
        {
            Type t = type.GetInterface(iface.Name);
            return t != null;
        }

        protected PickerType GetPickerType(PropertyInfo info)
        {
            Type type = info.PropertyType;
            switch (type.Name)
            {
                case "Date":
                case "DateTime":
                    return PickerType.DatePicker;
                case "FileInfo":
                    return PickerType.FilePicker;
                case "Font":
                    return PickerType.FontPicker;
                case "Color":
                    return PickerType.ColorPicker;
                case "String":
                    return PickerType.FilePicker;
                default:
                    return PickerType.ListPicker;
            }

        }

        private bool IsFlagsType(Type type)
        {
            return type.IsDefined(typeof(FlagsAttribute), false);
        }

        public static MaskAttribute GetMaskAttribute(PropertyInfo info)
        {
            MaskAttribute attr = (MaskAttribute)GetAttribute(typeof(MaskAttribute), info);
            return attr;
        }

        private bool HasMaskAttribute(PropertyInfo info)
        {
            return (GetMaskAttribute(info) != null);
        }

        public static FileNameAttribute GetFileNameAttribute(PropertyInfo info)
        {
            FileNameAttribute attr = (FileNameAttribute)GetAttribute(typeof(FileNameAttribute), info);
            return attr;
        }
        
        private bool HasFileNameAttribute(PropertyInfo info)
        {
            return (GetFileNameAttribute(info) != null);
        }

        protected LabelOption GetLabelOption(PropertyInfo info)
        {
            LabelOption option = LabelOption.Default;

            // does the property have a Display attribute?
            DisplayAttribute attr = GetDisplayAttribute(info);
            if (attr != null)
                option = attr.LabelOption;
            if (option != LabelOption.Default)
                return option;

            // does the type have a display attribute?
            Type type = info.PropertyType;
            attr = GetDisplayAttribute(type);
            if (attr != null)
                option = attr.LabelOption;
            if (option != LabelOption.Default)
                return option;


            switch (type.Name)
            {
                case "Boolean":
                    return LabelOption.None;
                case "String":
                    return LabelOption.Left;
                default:
                    if (IsEnumerable(type))
                        return LabelOption.Group;
                   return LabelOption.Left;
            }
        }

        private bool IsEnumerable(Type type)
        {
            foreach (var i in type.GetInterfaces())
            {
                if (i == typeof(System.Collections.IEnumerable))
                    return true;
            }
            return false;
        }

        protected DisplayAttribute GetDisplayAttribute(Type type)
        {
            Attribute attr = GetAttribute(typeof(DisplayAttribute), type);
            return (DisplayAttribute)attr;
        }

        protected static Attribute GetAttribute(Type attrType, MemberInfo info)
        {
            Attribute attr = null;
            object[] attributes = info.GetCustomAttributes(attrType, true);
            if (attributes != null && attributes.GetLength(0) > 0)
                attr = (Attribute)attributes[0];
            return attr;
        }

        protected DisplayAttribute GetDisplayAttribute(PropertyInfo info)
        {
            Attribute attr = GetAttribute(typeof(DisplayAttribute), info);
            return (DisplayAttribute)attr;
        }

        protected static string GetLabel(PropertyInfo info)
        {
            string label = null;
            DisplayNameAttribute attr = (DisplayNameAttribute)GetAttribute(typeof(DisplayNameAttribute), info);
            if (attr != null)
                label = attr.DisplayName;
            if( label == null )
                label = NaturalLanguageSystem.GetLabel(info);
            return label;
        }

        protected static RangeAttribute GetUIRangeAttribute(PropertyInfo info)
        {
            Attribute attr = GetAttribute(typeof(RangeAttribute), info);
            return (RangeAttribute)attr;
        }

        protected static IncrementAttribute GetUIIncrementAttribute(PropertyInfo info)
        {
            Attribute attr = GetAttribute(typeof(IncrementAttribute), info);
            return (IncrementAttribute)attr;
        }

        protected static Dictionary<string, object> GetComboListItems(PropertyInfo info)
        {
            Dictionary<string, object> dict = new Dictionary<string, object>();
            Type type = info.PropertyType;
            if (type.IsEnum)
            {
                Array values = type.GetEnumValues();
                foreach (object value in values)
                {
                    string key = type.GetEnumName(value);
                    dict.Add(key, value);
                }
            }
            return dict;
        }

 
    }
}
