﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CodeFirstUI.Framework
{
    [AttributeUsage(AttributeTargets.Property)]
    public class FileNameAttribute : Attribute
    {

        public string Filter { get; set; }
        public bool ForSave { get; set; }

        public FileNameAttribute()
        {
            this.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
            this.ForSave = false;
        }

        public FileNameAttribute(string filter, bool forSave)
        {
            this.Filter = filter;
            this.ForSave = forSave;
        }
    }
}
