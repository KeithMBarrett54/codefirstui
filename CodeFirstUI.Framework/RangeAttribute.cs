﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CodeFirstUI.Framework
{
    public class RangeAttribute : Attribute
    {
        public decimal Maximum { get; set; }
        public decimal Minimum { get; set; }

        public RangeAttribute(double min, double max)
        {
            Minimum = (decimal)min;
            Maximum = (decimal)max;
        }

    }
}
